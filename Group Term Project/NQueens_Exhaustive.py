from itertools import combinations
import copy
import sys
import time


class NQueens_Exhaustive:
    def __init__(self, size):
        self.size = size
        self.solve()

    def solve(self):
        all = []
        for a in range(1, self.size+1):
            for b in range(1, self.size+1):
                all.append([a, b])
        temp = list(combinations(all, self.size))

        self.solutions = []
        for s in temp:
            if self.checkQueenAttack(s) == False:
                self.solutions.append(s)

    def checkQueenAttack(self, solution):
        checkRow = [0] * self.size
        checkColumn = [0] * self.size
        for queen in solution:
            checkRow[queen[0]-1] = 1
            checkColumn[queen[1]-1] = 1
        if (0 in checkRow) or (0 in checkColumn):
            return True
        else:
            temp = list(combinations(solution, 2))
            for i in temp:
                if (abs(i[0][0]-i[1][0])) == (abs(i[0][1]-i[1][1])):
                    return True
            return False

    def printBoard(self, solution):
        size = len(solution)
        board = []
        for i in range(0, size):
            board.append([0] * size)
        for queen in solution:
            board[queen[0]-1][queen[1]-1] = 1
        for row in board:
            for column in row:
                if column == 0:
                    print("□", end=" ")
                else:
                    print("■", end=" ")
            print()

    def printAllSolutions(self):
        for s in self.solutions:
            self.printBoard(s)
            print()
        print("共有", len(self.solutions), "個解")


def main():

    size = 4
    if len(sys.argv) >= 2:
        size = int(sys.argv[1])

    tStart = time.time()
    s = NQueens_Exhaustive(size)
    s.printAllSolutions()
    # print(s.solutions)
    tEnd = time.time()

    print(round(tEnd - tStart, 4), "seconds")


if __name__ == "__main__":
    main()
