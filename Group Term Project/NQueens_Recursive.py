import sys
import time


class NQueens_Recursive:
    def __init__(self, size):
        self.size = size
        self.solutions = []
        self.solve()

    def solve(self):
        positions = [-1] * self.size
        self.putNextQueen(positions, 0)

    def putNextQueen(self, positions, nowRow):
        if nowRow == self.size:
            self.solutions.append(self.positionsToSolution(positions))
        else:
            for column in range(self.size):
                if self.canPutQueen(positions, nowRow, column):
                    positions[nowRow] = column
                    self.putNextQueen(positions, nowRow + 1)

    def canPutQueen(self, positions, nowRow, column):
        for i in range(0, nowRow):
            if (positions[i] == column) or (abs(nowRow - i) == abs(column - positions[i])):
                return False
        return True

    def positionsToSolution(self, positions):
        solution = []
        for row in range(self.size):
            solution.append([row+1, positions[row]+1])
        return solution

    def printBoard(self, solution):
        size = len(solution)
        board = []
        for i in range(0, size):
            board.append([0] * size)
        for queen in solution:
            board[queen[0]-1][queen[1]-1] = 1
        for row in board:
            for column in row:
                if column == 0:
                    print("□", end=" ")
                else:
                    print("■", end=" ")
            print()

    def printAllSolutions(self):
        for s in self.solutions:
            self.printBoard(s)
            print()
        print("共有", len(self.solutions), "個解")


def main():

    size = 4
    if len(sys.argv) >= 2:
        size = int(sys.argv[1])

    tStart = time.time()
    s = NQueens_Recursive(size)
    s.printAllSolutions()
    # print(s.solutions)
    tEnd = time.time()

    print(round(tEnd - tStart, 4), "seconds")


if __name__ == "__main__":
    main()
