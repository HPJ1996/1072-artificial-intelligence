from itertools import combinations
import sys
import math


class NQueens_Special:
    def __init__(self, size):
        self.size = size
        self.solutions = []
        self.solve()

    def solve(self):
        positions = [-1] * self.size
        for startColumn in range(self.size):
            column = startColumn
            for i in range(0, self.size):
                positions[i] = column % self.size
                column += 2
            if self.checkQueenAttack(self.positionsToSolution(positions)) == False:
                self.solutions.append(self.positionsToSolution(positions))

    def checkQueenAttack(self, solution):
        checkRow = [0] * self.size
        checkColumn = [0] * self.size
        for queen in solution:
            checkRow[queen[0]-1] = 1
            checkColumn[queen[1]-1] = 1
        if (0 in checkRow) or (0 in checkColumn):
            return True
        else:
            temp = list(combinations(solution, 2))
            for i in temp:
                if (abs(i[0][0]-i[1][0])) == (abs(i[0][1]-i[1][1])):
                    return True
            return False

    def positionsToSolution(self, positions):
        solution = []
        for row in range(self.size):
            solution.append([row+1, positions[row]+1])
        return solution

    def printBoard(self, solution):
        size = len(solution)
        board = []
        for i in range(0, size):
            board.append([0] * size)
        for queen in solution:
            board[queen[0]-1][queen[1]-1] = 1
        for row in board:
            for column in row:
                if column == 0:
                    print("□", end=" ")
                else:
                    print("■", end=" ")
            print()

    def printAllSolutions(self):
        for s in self.solutions:
            self.printBoard(s)
            print()
        print("共有", len(self.solutions), "個解")


def main():

    size = 4
    if len(sys.argv) >= 2:
        size = int(sys.argv[1])

    s = NQueens_Special(size)
    s.printAllSolutions()
    print(len(s.solutions))
    print()


if __name__ == "__main__":
    main()
