from itertools import combinations
import copy
import sys


class NQueens_Recursive:
    def __init__(self, size):
        self.size = size
        self.solutions = []
        self.solve()

    def solve(self):
        positions = [-1] * self.size
        self.putNextQueen(positions, 0)

    def putNextQueen(self, positions, nowRow):
        if nowRow == self.size:
            self.solutions.append(self.positionsToSolution(positions))
        else:
            for column in range(self.size):
                if self.canPutQueen(positions, nowRow, column):
                    positions[nowRow] = column
                    self.putNextQueen(positions, nowRow + 1)

    def canPutQueen(self, positions, nowRow, column):
        for i in range(0, nowRow):
            if (positions[i] == column) or (abs(nowRow - i) == abs(column - positions[i])):
                return False
        return True

    def positionsToSolution(self, positions):
        solution = []
        for row in range(self.size):
            solution.append([row+1, positions[row]+1])
        return solution

    def printBoard(self, solution):
        size = len(solution)
        board = []
        for i in range(0, size):
            board.append([0] * size)
        for queen in solution:
            board[queen[0]-1][queen[1]-1] = 1
        for row in board:
            for column in row:
                if column == 0:
                    print("□", end=" ")
                else:
                    print("■", end=" ")
            print()

    def printAllSolutions(self):
        for s in self.solutions:
            self.printBoard(s)
            print()
        print("共有", len(self.solutions), "個解")


class Merge_Solutions:
    def __init__(self, solutionsA, solutionsB):
        self.solutionsA = solutionsA
        self.solutionsB = solutionsB
        self.newSolutions = []

    def mergeSolutions(self, solutionsA, solutionsB, sizeA, sizeB):
        newSolutions = []
        for s1 in solutionsA:
            temp1 = []
            for queen in s1:
                temp1.append(queen)
            for s2 in solutionsB:
                temp2 = copy.deepcopy(temp1)
                for queen in s2:
                    temp2.append([i+sizeA for i in queen])
                newSolutions.append(temp2)
        return newSolutions

    def checkQueenAttack(self, solution):
        checkRow = [0] * len(solution)
        checkColumn = [0] * len(solution)
        for queen in solution:
            checkRow[queen[0]-1] = 1
            checkColumn[queen[1]-1] = 1
        if (0 in checkRow) or (0 in checkColumn):
            return True
        else:
            temp = list(combinations(solution, 2))
            for i in temp:
                if (abs(i[0][0]-i[1][0])) == (abs(i[0][1]-i[1][1])):
                    return True
            return False

    def printBoard(self, solution):
        size = len(solution)
        board = []
        for i in range(0, size):
            board.append([0] * size)
        for queen in solution:
            board[queen[0]-1][queen[1]-1] = 1
        for row in board:
            for column in row:
                if column == 0:
                    print("□", end=" ")
                else:
                    print("■", end=" ")
            print()

    def solve(self):
        self.newSolutions = []
        temp = self.mergeSolutions(self.solutionsA, self.solutionsB, len(self.solutionsA[0]), len(self.solutionsB[0]))
        for solution in temp:
            if self.checkQueenAttack(solution) == False:
                self.newSolutions.append(solution)
                print(len(self.solutionsA[0]), "+", len(self.solutionsB[0]))
                self.printBoard(solution)
                print()


def main():

    solutionsA = NQueens_Recursive(int(sys.argv[1])).solutions
    solutionsB = NQueens_Recursive(int(sys.argv[2])).solutions

    newSolutions = Merge_Solutions(solutionsA, solutionsB)
    newSolutions.solve()
    print(len(newSolutions.newSolutions))


if __name__ == "__main__":
    main()
