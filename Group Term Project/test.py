from itertools import combinations
import copy


def printBoard(solutions):
    size = len(solutions)
    board = []
    for i in range(0, size):
        board.append([0] * size)
    for queen in solutions:
        board[queen[0]-1][queen[1]-1] = 1
    for row in board:
        for column in row:
            if column == 0:
                print("□", end=" ")
            else:
                print("■", end=" ")
        print()


def checkQueenAttack(solutions):
    size = len(solutions)
    checkRow = [0] * size
    checkColumn = [0] * size
    for queen in solutions:
        checkRow[queen[0]-1] = 1
        checkColumn[queen[1]-1] = 1
    if (0 in checkRow) or (0 in checkColumn):
        return True
    else:
        temp = list(combinations(solutions, 2))
        for i in temp:
            if (abs(i[0][0]-i[1][0])) == (abs(i[0][1]-i[1][1])):
                return True
        return False


def main(size):
    print("產生", size, "Queen 的所有解")
    all = []
    for a in range(1, size+1):
        for b in range(1, size+1):
            all.append([a, b])
    temp = list(combinations(all, size))

    solutions = []
    for s in temp:
        if checkQueenAttack(s) == False:
            solutions.append(s)
            printBoard(s)
            print()
    # print(solutions)
    print("共有", len(solutions), "個解")


def mergeSolutions(solutionsA, solutionsB, sizeA, sizeB):
    newSolutions = []
    for s1 in solutionsA:
        temp1 = []
        for queen in s1:
            temp1.append(queen)
        for s2 in solutionsB:
            temp2 = copy.deepcopy(temp1)
            for queen in s2:
                temp2.append([i+sizeA for i in queen])
            newSolutions.append(temp2)
    return newSolutions


def main2():
    print("N Queen 團體作業")
    solutions = []
    solutions.append(solutions1)
    solutions.append(solutions4)
    solutions.append(solutions5)
    solutions.append(solutions6)
    solutions.append(solutions7)

    newSolutions = []

    for aBoard in solutions:
        for bBoard in solutions:
            temp = mergeSolutions(aBoard, bBoard, len(aBoard[0]), len(bBoard[0]))
            for newSolution in temp:
                if checkQueenAttack(newSolution) == False:
                    newSolutions.append(newSolutions)
                    print(len(aBoard[0]), "+", len(bBoard[0]))
                    printBoard(newSolution)
                    print()


def printAllSolutions(solutions):
    for s in solutions:
        printBoard(s)
        print()


# ==================================================

main(1)
main(5)

solutions1 = [[([1, 1])]]
solutions2 = []
solutions3 = []
solutions4 = [([1, 2], [2, 4], [3, 1], [4, 3]), ([1, 3], [2, 1], [3, 4], [4, 2])]
solutions5 = [([1, 1], [2, 3], [3, 5], [4, 2], [5, 4]), ([1, 1], [2, 4], [3, 2], [4, 5], [5, 3]), ([1, 2], [2, 4], [3, 1], [4, 3], [5, 5]), ([1, 2], [2, 5], [3, 3], [4, 1], [5, 4]), ([1, 3], [2, 1], [3, 4], [4, 2], [5, 5]), ([1, 3], [2, 5], [3, 2], [4, 4], [5, 1]), ([1, 4], [2, 1], [3, 3], [4, 5], [5, 2]), ([1, 4], [2, 2], [3, 5], [4, 3], [5, 1]), ([1, 5], [2, 2], [3, 4], [4, 1], [5, 3]), ([1, 5], [2, 3], [3, 1], [4, 4], [5, 2])]
solutions6 = [([1, 2], [2, 4], [3, 6], [4, 1], [5, 3], [6, 5]), ([1, 3], [2, 6], [3, 2], [4, 5], [5, 1], [6, 4]), ([1, 4], [2, 1], [3, 5], [4, 2], [5, 6], [6, 3]), ([1, 5], [2, 3], [3, 1], [4, 6], [5, 4], [6, 2])]
solutions7 = [([1, 1], [2, 3], [3, 5], [4, 7], [5, 2], [6, 4], [7, 6]), ([1, 1], [2, 4], [3, 7], [4, 3], [5, 6], [6, 2], [7, 5]), ([1, 1], [2, 5], [3, 2], [4, 6], [5, 3], [6, 7], [7, 4]), ([1, 1], [2, 6], [3, 4], [4, 2], [5, 7], [6, 5], [7, 3]), ([1, 2], [2, 4], [3, 1], [4, 7], [5, 5], [6, 3], [7, 6]), ([1, 2], [2, 4], [3, 6], [4, 1], [5, 3], [6, 5], [7, 7]), ([1, 2], [2, 5], [3, 1], [4, 4], [5, 7], [6, 3], [7, 6]), ([1, 2], [2, 5], [3, 3], [4, 1], [5, 7], [6, 4], [7, 6]), ([1, 2], [2, 5], [3, 7], [4, 4], [5, 1], [6, 3], [7, 6]), ([1, 2], [2, 6], [3, 3], [4, 7], [5, 4], [6, 1], [7, 5]), ([1, 2], [2, 7], [3, 5], [4, 3], [5, 1], [6, 6], [7, 4]), ([1, 3], [2, 1], [3, 6], [4, 2], [5, 5], [6, 7], [7, 4]), ([1, 3], [2, 1], [3, 6], [4, 4], [5, 2], [6, 7], [7, 5]), ([1, 3], [2, 5], [3, 7], [4, 2], [5, 4], [6, 6], [7, 1]), ([1, 3], [2, 6], [3, 2], [4, 5], [5, 1], [6, 4], [7, 7]), ([1, 3], [2, 7], [3, 2], [4, 4], [5, 6], [6, 1], [7, 5]), ([1, 3], [2, 7], [3, 4], [4, 1], [5, 5], [6, 2], [7, 6]), ([1, 4], [2, 1], [3, 3], [4, 6], [5, 2], [6, 7], [7, 5]), ([1, 4], [2, 1], [3, 5], [4, 2], [5, 6], [6, 3], [7, 7]), ([1, 4], [2, 2], [3, 7], [4, 5], [5, 3], [6, 1], [7, 6]), ([1, 4], [2, 6], [3, 1], [4, 3], [5, 5], [6, 7], [7, 2]), ([1, 4], [2, 7], [3, 3], [4, 6], [5, 2], [6, 5], [7, 1]), ([1, 4], [2, 7], [3, 5], [4, 2], [5, 6], [6, 1], [7, 3]), ([1, 5], [2, 1], [3, 4], [4, 7], [5, 3], [6, 6], [7, 2]), ([1, 5], [2, 1], [3, 6], [4, 4], [5, 2], [6, 7], [7, 3]), ([1, 5], [2, 2], [3, 6], [4, 3], [5, 7], [6, 4], [7, 1]), ([1, 5], [2, 3], [3, 1], [4, 6], [5, 4], [6, 2], [7, 7]), ([1, 5], [2, 7], [3, 2], [4, 4], [5, 6], [6, 1], [7, 3]), ([1, 5], [2, 7], [3, 2], [4, 6], [5, 3], [6, 1], [7, 4]), ([1, 6], [2, 1], [3, 3], [4, 5], [5, 7], [6, 2], [7, 4]), ([1, 6], [2, 2], [3, 5], [4, 1], [5, 4], [6, 7], [7, 3]), ([1, 6], [2, 3], [3, 1], [4, 4], [5, 7], [6, 5], [7, 2]), ([1, 6], [2, 3], [3, 5], [4, 7], [5, 1], [6, 4], [7, 2]), ([1, 6], [2, 3], [3, 7], [4, 4], [5, 1], [6, 5], [7, 2]), ([1, 6], [2, 4], [3, 2], [4, 7], [5, 5], [6, 3], [7, 1]), ([1, 6], [2, 4], [3, 7], [4, 1], [5, 3], [6, 5], [7, 2]), ([1, 7], [2, 2], [3, 4], [4, 6], [5, 1], [6, 3], [7, 5]), ([1, 7], [2, 3], [3, 6], [4, 2], [5, 5], [6, 1], [7, 4]), ([1, 7], [2, 4], [3, 1], [4, 5], [5, 2], [6, 6], [7, 3]), ([1, 7], [2, 5], [3, 3], [4, 1], [5, 6], [6, 4], [7, 2])]

printAllSolutions(solutions4)
printAllSolutions(solutions7)

main2()
