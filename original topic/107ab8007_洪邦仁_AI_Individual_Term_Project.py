import copy

LAYER = 2
GRID = 9
BLANK = "blank"


def printState(s):
    print("╔═══╦═══╦═══╗   ╔═══╦═══╦═══╗")
    print("║", s[0][0], "║", s[0][1], "║", s[0][2],
          "║   ║", s[1][0], "║", s[1][1], "║", s[1][2], "║")
    print("╠═══╬═══╬═══╣   ╠═══╬═══╬═══╣")
    print("║", s[0][3], "║", s[0][4], "║", s[0][5],
          "║   ║", s[1][3], "║", s[1][4], "║", s[1][5], "║")
    print("╠═══╬═══╬═══╣   ╠═══╬═══╬═══╣")
    print("║", s[0][6], "║", s[0][7], "║", s[0][8],
          "║   ║", s[1][6], "║", s[1][7], "║", s[1][8], "║")
    print("╚═══╩═══╩═══╝   ╚═══╩═══╩═══╝")


def printAllState(ts):
    count = 0
    for s in ts:
        print(count, "step:")
        printState(s)
        count += 1
        print()


def destinationPlace(now, goal):
    temp = []
    temp.append([0] * GRID)
    temp.append([0] * GRID)

    for layer in range(0, LAYER):
        for i in range(0, GRID):
            if now[layer][i] == ' ':
                temp[layer][i] = BLANK
            else:
                if now[layer][i] == goal[layer][i]:
                    temp[layer][i] = True
                else:
                    for layer2 in range(0, LAYER):
                        if now[layer][i] in goal[layer2]:
                            temp[layer][i] = [
                                layer2, goal[layer2].index(now[layer][i])]
    return temp


def isNotBlank(now):
    temp = []
    for layer in range(0, LAYER):
        for i in range(0, GRID):
            if now[layer][i] != ' ':
                temp.append([layer, i])
    return temp


def canMove(now):
    data = [[[now[0], 1], [now[0], 3]],
            [[now[0], 0], [now[0], 2], [now[0], 4]],
            [[now[0], 1], [now[0], 5]],
            [[now[0], 0], [now[0], 4], [now[0], 6]],
            [[now[0], 1], [now[0], 3], [now[0], 5],
                [now[0], 7], [LAYER-1-now[0], 4]],
            [[now[0], 2], [now[0], 4], [now[0], 8]],
            [[now[0], 3], [now[0], 7]],
            [[now[0], 4], [now[0], 6], [now[0], 8]],
            [[now[0], 5], [now[0], 7]]]
    return data[now[1]]


def heuristicFunction(now, goal):
    sum = 0
    dp = destinationPlace(now, goal)
    for layer in range(0, LAYER):
        for i in range(0, GRID):
            if (dp[layer][i] != True) and (dp[layer][i] != BLANK):
                if dp[layer][i][0] == layer:
                    sum += calculateDistance(i, dp[layer][i][1])
                else:
                    sum += calculateDistance(i, 4)
                    sum += calculateDistance(4, dp[layer][i][1])
                    sum += 1
    return sum


def toRowColumn(n):
    rc = [[1, 1], [1, 2], [1, 3], [2, 1],
          [2, 2], [2, 3], [3, 1], [3, 2], [3, 3]]
    return rc[n]


def calculateDistance(a, b):
    return abs(toRowColumn(a)[0] - toRowColumn(b)[0]) + abs(toRowColumn(a)[1] - toRowColumn(b)[1])


def isComplete(now, goal):
    dp = destinationPlace(now, goal)
    for layer in range(0, LAYER):
        if (dp[0].count(True) + dp[0].count(BLANK)) != GRID:
            return False
    return True


def solve(initial, goal):
    totalStep = []
    totalStep.append(initial)

    while totalStep[-1] != goal:

        possibleStep = []
        for fromP in isNotBlank(totalStep[-1]):
            for toP in canMove(fromP):
                if(totalStep[-1][toP[0]][toP[1]] == ' '):
                    tempStep = copy.deepcopy(totalStep[-1])
                    tempStep[toP[0]][toP[1]] = tempStep[fromP[0]][fromP[1]]
                    tempStep[fromP[0]][fromP[1]] = ' '
                    tempHF = heuristicFunction(tempStep, goal)
                    possibleStep.append([tempHF, tempStep])
        index = 0
        while sorted(possibleStep)[index][1] in totalStep:
            index += 1
        totalStep.append(sorted(possibleStep)[index][1])

    return totalStep


def main():
    print("107AB8007 洪邦仁 雙層九宮格作業")
    initial = [['1', '3', '6', '5', ' ', ' ', 'A', 'B', '7'],
               ['2', 'C', 'D', ' ', ' ', 'E', '4', 'G', 'F']]
    goal = [['3', '4', '5', '2', ' ', '6', '1', ' ', '7'],
            ['C', 'D', 'E', 'B', ' ', 'F', 'A', ' ', 'G']]
    printAllState(solve(initial, goal))


main()
